import Vue from 'vue'
import Router from 'vue-router'
import Game from '@/components/Game'
import Results from '@/components/Results'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'game',
      component: Game
    },
    {
      path: '/results/:percent',
      name: 'results',
      component: Results
    }
  ]
})
